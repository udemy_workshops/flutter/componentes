import 'package:flutter/material.dart';

class HomePageTemp extends StatelessWidget {
  final List<String> opciones = ['Uno', 'Dos', 'Tres', 'Cuatro', 'Cinco'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Componentes Temp',
        ),
      ),
      body: ListView(
        children: _crearItemsCorto(),
      ),
    );
  }

  List<Widget> _crearItems() {
    List<Widget> lista = new List<Widget>();

    for (String opcion in opciones) {
      if (lista.length > 0) {
        lista.add(
          Divider(
            color: Colors.green,
          ),
        );
      }
      lista.add(ListTile(
        title: Text(
          opcion,
        ),
      ));
    }
    return lista;
  }

  List<Widget> _crearItemsCorto() {
    return opciones.map((opcion) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text('$opcion'),
            subtitle: Text('Cualquier cosa'),
            leading: Icon(
              Icons.person,
            ),
            trailing: Icon(
              Icons.arrow_right,
            ),
            onTap: () {},
          ),
          Divider(),
        ],
      );
    }).toList();
  }

}